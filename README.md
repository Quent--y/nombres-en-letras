# Nombres en letras



## Cossí utilizar lo fichièr

Lo fichièr .SOR servís al programa [Numbertext](https://numbertext.github.io/) per generat l’escritura en letra de nombres e prèses.

## Foncionalitats e limits
- generar nombres
- generar somas (Èuro, libra esterlina, dòlar estatsunidenc, dòlar australian, franc soís, ien, franc andorran, peseta andorrana, peseta espanhòla, franc francés, lira italiana, lira italiana, franc monegasc, escut portugués, lira vaticana)
- generar ordinals masculins e feminins
- generar ordinals corts
- presa en compte del feminin de Un e Dos
- apostrofe de « de » devant : èuro, ien e escut
- apondon del « de » als nombres superiors a un milion pels nombres redonds
- fin de l’acòrd de Un e Dos davant un nombre compausat al dessús de mila
- /!\ en cas d’apondon d’una moneda que fas "as", serà considerada coma feminina, coma los « francs belgas »
- /!\ bilion, trilion, quadrilion son pas preses en carga pels ordinals


## Autor
Quentin PAGÈS

## Licéncia
Tota utilizacion tan que l’autor es mencionat.
